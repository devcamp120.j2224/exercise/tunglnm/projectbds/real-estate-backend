package com.devcamp.project.realestate.service;

import java.util.*;

import com.devcamp.project.realestate.model.*;
import com.devcamp.project.realestate.repository.IMasterLayoutRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MasterLayoutService {
    @Autowired
    IMasterLayoutRepository pMasterLayoutRepository;

    public List<CMasterLayout> getMasterLayoutList() {
        List<CMasterLayout> masterLayoutList = new ArrayList<CMasterLayout>();
        pMasterLayoutRepository.findAll().forEach(masterLayoutList::add);
        return masterLayoutList;
    }

    public Object createMasterLayoutObj(CMasterLayout cMasterLayout, Optional<CProject> cProject) {

        CMasterLayout newMasterLayout = new CMasterLayout();
        newMasterLayout.setName(cMasterLayout.getName());
        newMasterLayout.setDescription(cMasterLayout.getDescription());
        newMasterLayout.setAcreage(cMasterLayout.getAcreage());
        newMasterLayout.setApartmentList(cMasterLayout.getApartmentList());
        newMasterLayout.setPhoto(cMasterLayout.getPhoto());
        newMasterLayout.setDateCreate(new Date());

        CProject _project = cProject.get();
        newMasterLayout.setProject(_project);

        CMasterLayout savedMasterLayout = pMasterLayoutRepository.save(newMasterLayout);
        return savedMasterLayout;
    }

    public Object updateMasterLayoutObj(CMasterLayout cMasterLayout, Optional<CMasterLayout> cMasterLayoutData) {

        CMasterLayout newMasterLayout = cMasterLayoutData.get();
        newMasterLayout.setName(cMasterLayout.getName());
        newMasterLayout.setDescription(cMasterLayout.getDescription());
        newMasterLayout.setAcreage(cMasterLayout.getAcreage());
        newMasterLayout.setApartmentList(cMasterLayout.getApartmentList());
        newMasterLayout.setPhoto(cMasterLayout.getPhoto());
        newMasterLayout.setDateUpdate(new Date());

        CMasterLayout savedMasterLayout = pMasterLayoutRepository.save(newMasterLayout);
        return savedMasterLayout;
    }
}
