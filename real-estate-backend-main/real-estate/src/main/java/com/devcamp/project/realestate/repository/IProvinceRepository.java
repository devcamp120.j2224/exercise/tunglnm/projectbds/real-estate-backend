package com.devcamp.project.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.project.realestate.model.*;
import org.springframework.stereotype.Repository;

@Repository
public interface IProvinceRepository extends JpaRepository<CProvince, Integer> {
    
}
