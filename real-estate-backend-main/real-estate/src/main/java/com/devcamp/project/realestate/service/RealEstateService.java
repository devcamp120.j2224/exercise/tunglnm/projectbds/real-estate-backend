package com.devcamp.project.realestate.service;

import java.util.*;

import com.devcamp.project.realestate.model.*;
import com.devcamp.project.realestate.repository.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RealEstateService {
    @Autowired
    IEmployeeRepository pEmployeeRepository;

    @Autowired
    IRealEstateRepository pRealEstateRepository;

    @Autowired
    IProjectRepository pProjectRepository;

    @Autowired
    IProvinceRepository pProvinceRepository;

    @Autowired
    IDistrictRepository pIDistrictRepository;

    @Autowired
    IWardRepository pIWardRepository;

    @Autowired
    IStreetRepository pIStreetRepository;

    @Autowired
    ICustomerRepository pICustomerRepository;

    public List<CRealEstate> getRealEstateList() {
        List<CRealEstate> realEstateList = new ArrayList<CRealEstate>();
        pRealEstateRepository.findAll().forEach(realEstateList::add);
        return realEstateList;
    }

    public Object createRealEstateObj(CRealEstate cRealEstate, Integer provinceId, Integer districtId, Integer wardId,
            Integer streetId, Integer projectId, Integer customerId, Integer employeeId) {

        Optional<CProvince> provinceData = pProvinceRepository.findById(provinceId);
        Optional<CDistrict> districtData = pIDistrictRepository.findById(districtId);
        Optional<CWard> wardData = pIWardRepository.findById(wardId);
        Optional<CStreet> streetData = pIStreetRepository.findById(streetId);
        Optional<CProject> projectData = pProjectRepository.findById(projectId);
        Optional<CCustomer> customerData = pICustomerRepository.findById(customerId);
        Optional<CEmployees> employeeData = pEmployeeRepository.findById(employeeId);

        CRealEstate newRealEstate = new CRealEstate();
        newRealEstate.setTitle(cRealEstate.getTitle());
        newRealEstate.setType(cRealEstate.getType());
        newRealEstate.setRequest(cRealEstate.getRequest());
        newRealEstate.setAddress(cRealEstate.getAddress());
        newRealEstate.setPrice(cRealEstate.getPrice());
        newRealEstate.setPriceMin(cRealEstate.getPriceMin());
        newRealEstate.setPriceTime(cRealEstate.getPriceTime());
        newRealEstate.setDateCreate(new Date());
        newRealEstate.setAcreage(cRealEstate.getAcreage());
        newRealEstate.setDirection(cRealEstate.getDirection());

        newRealEstate.setTotalFloors(cRealEstate.getTotalFloors());
        newRealEstate.setNumberFloors(cRealEstate.getNumberFloors());
        newRealEstate.setBath(cRealEstate.getBath());
        newRealEstate.setApartCode(cRealEstate.getApartCode());
        newRealEstate.setWallArea(cRealEstate.getWallArea());
        newRealEstate.setBedroom(cRealEstate.getBedroom());
        newRealEstate.setBalcony(cRealEstate.getBalcony());
        newRealEstate.setLandscapeView(cRealEstate.getLandscapeView());
        newRealEstate.setApartLoca(cRealEstate.getApartLoca());
        newRealEstate.setApartType(cRealEstate.getApartType());

        newRealEstate.setFurnitureType(cRealEstate.getFurnitureType());
        newRealEstate.setPriceRent(cRealEstate.getPriceRent());
        newRealEstate.setReturnRate(cRealEstate.getReturnRate());
        newRealEstate.setLegalDoc(cRealEstate.getLegalDoc());
        newRealEstate.setDescription(cRealEstate.getDescription());
        newRealEstate.setWidthY(cRealEstate.getWidthY());
        newRealEstate.setLongX(cRealEstate.getLongX());
        newRealEstate.setStreetHouse(cRealEstate.getStreetHouse());
        newRealEstate.setFSBO(cRealEstate.getFSBO());
        newRealEstate.setViewNum(cRealEstate.getViewNum());

        newRealEstate.setUpdateBy(cRealEstate.getUpdateBy());
        newRealEstate.setShape(cRealEstate.getShape());
        newRealEstate.setDistance2facade(cRealEstate.getDistance2facade());
        newRealEstate.setAdjacentRoad(cRealEstate.getAdjacentRoad());
        newRealEstate.setAdjacent_facade_num(cRealEstate.getAdjacent_facade_num());
        newRealEstate.setAlley_min_width(cRealEstate.getAlley_min_width());
        newRealEstate.setAdjacent_alley_min_width(cRealEstate.getAdjacent_alley_min_width());
        newRealEstate.setFactor(cRealEstate.getFactor());
        newRealEstate.setStructure(cRealEstate.getStructure());

        newRealEstate.setDTSXD(cRealEstate.getDTSXD());
        newRealEstate.setCLCL(cRealEstate.getCLCL());
        newRealEstate.setCTXD_price(cRealEstate.getCTXD_price());
        newRealEstate.setCTXD_value(cRealEstate.getCTXD_value());
        newRealEstate.setPhoto(cRealEstate.getPhoto());
        newRealEstate.setLat(cRealEstate.getLat());
        newRealEstate.setLng(cRealEstate.getLng());
        newRealEstate.setStatus("N");

        if (provinceData.isPresent()) {
            CProvince _province = provinceData.get();
            newRealEstate.setProvince(_province);
        } else {
            newRealEstate.setProvince(null);
        }

        if (districtData.isPresent()) {
            CDistrict _district = districtData.get();
            newRealEstate.setDistrict(_district);
        } else {
            newRealEstate.setDistrict(null);
        }

        if (wardData.isPresent()) {
            CWard _ward = wardData.get();
            newRealEstate.setWards(_ward);
        } else {
            newRealEstate.setWards(null);
        }

        if (streetData.isPresent()) {
            CStreet _street = streetData.get();
            newRealEstate.setStreet(_street);
        } else {
            newRealEstate.setStreet(null);
        }

        if (projectData.isPresent()) {
            CProject _project = projectData.get();
            newRealEstate.setProject(_project);
        } else {
            newRealEstate.setProject(null);
        }

        if (customerData.isPresent()) {
            CCustomer _customer = customerData.get();
            newRealEstate.setCustomer(_customer);
        } else {
            newRealEstate.setCustomer(null);
        }

        if (employeeData.isPresent()) {
            CEmployees _employee = employeeData.get();
            newRealEstate.setEmployee(_employee);
        } else {
            newRealEstate.setCustomer(null);
        }

        CRealEstate savedRealEstate = pRealEstateRepository.save(newRealEstate);
        return savedRealEstate;
    }

    public Object updateRealEstateObj(CRealEstate cRealEstate, Optional<CRealEstate> realEstateData, Integer provinceId, Integer districtId, Integer wardId,
            Integer streetId, Integer projectId, Integer customerId, Integer employeeId) {

        Optional<CProvince> provinceData = pProvinceRepository.findById(provinceId);
        Optional<CDistrict> districtData = pIDistrictRepository.findById(districtId);
        Optional<CWard> wardData = pIWardRepository.findById(wardId);
        Optional<CStreet> streetData = pIStreetRepository.findById(streetId);
        Optional<CProject> projectData = pProjectRepository.findById(projectId);
        Optional<CCustomer> customerData = pICustomerRepository.findById(customerId);
        Optional<CEmployees> employeeData = pEmployeeRepository.findById(employeeId);

        CRealEstate newRealEstate = realEstateData.get();
        newRealEstate.setTitle(cRealEstate.getTitle());
        newRealEstate.setType(cRealEstate.getType());
        newRealEstate.setRequest(cRealEstate.getRequest());
        newRealEstate.setAddress(cRealEstate.getAddress());
        newRealEstate.setPrice(cRealEstate.getPrice());
        newRealEstate.setPriceMin(cRealEstate.getPriceMin());
        newRealEstate.setPriceTime(cRealEstate.getPriceTime());
        newRealEstate.setAcreage(cRealEstate.getAcreage());
        newRealEstate.setDirection(cRealEstate.getDirection());

        newRealEstate.setTotalFloors(cRealEstate.getTotalFloors());
        newRealEstate.setNumberFloors(cRealEstate.getNumberFloors());
        newRealEstate.setBath(cRealEstate.getBath());
        newRealEstate.setApartCode(cRealEstate.getApartCode());
        newRealEstate.setWallArea(cRealEstate.getWallArea());
        newRealEstate.setBedroom(cRealEstate.getBedroom());
        newRealEstate.setBalcony(cRealEstate.getBalcony());
        newRealEstate.setLandscapeView(cRealEstate.getLandscapeView());
        newRealEstate.setApartLoca(cRealEstate.getApartLoca());
        newRealEstate.setApartType(cRealEstate.getApartType());

        newRealEstate.setFurnitureType(cRealEstate.getFurnitureType());
        newRealEstate.setPriceRent(cRealEstate.getPriceRent());
        newRealEstate.setReturnRate(cRealEstate.getReturnRate());
        newRealEstate.setLegalDoc(cRealEstate.getLegalDoc());
        newRealEstate.setDescription(cRealEstate.getDescription());
        newRealEstate.setWidthY(cRealEstate.getWidthY());
        newRealEstate.setLongX(cRealEstate.getLongX());
        newRealEstate.setStreetHouse(cRealEstate.getStreetHouse());
        newRealEstate.setFSBO(cRealEstate.getFSBO());
        newRealEstate.setViewNum(cRealEstate.getViewNum());

        newRealEstate.setUpdateBy(cRealEstate.getUpdateBy());
        newRealEstate.setShape(cRealEstate.getShape());
        newRealEstate.setDistance2facade(cRealEstate.getDistance2facade());
        newRealEstate.setAdjacentRoad(cRealEstate.getAdjacentRoad());
        newRealEstate.setAdjacent_facade_num(cRealEstate.getAdjacent_facade_num());
        newRealEstate.setAlley_min_width(cRealEstate.getAlley_min_width());
        newRealEstate.setAdjacent_alley_min_width(cRealEstate.getAdjacent_alley_min_width());
        newRealEstate.setFactor(cRealEstate.getFactor());
        newRealEstate.setStructure(cRealEstate.getStructure());

        newRealEstate.setDTSXD(cRealEstate.getDTSXD());
        newRealEstate.setCLCL(cRealEstate.getCLCL());
        newRealEstate.setCTXD_price(cRealEstate.getCTXD_price());
        newRealEstate.setCTXD_value(cRealEstate.getCTXD_value());
        newRealEstate.setPhoto(cRealEstate.getPhoto());
        newRealEstate.setLat(cRealEstate.getLat());
        newRealEstate.setLng(cRealEstate.getLng());
        newRealEstate.setStatus(cRealEstate.getStatus());

        if (provinceData.isPresent()) {
            CProvince _province = provinceData.get();
            newRealEstate.setProvince(_province);
        } else {
            newRealEstate.setProvince(null);
        }

        if (districtData.isPresent()) {
            CDistrict _district = districtData.get();
            newRealEstate.setDistrict(_district);
        } else {
            newRealEstate.setDistrict(null);
        }

        if (wardData.isPresent()) {
            CWard _ward = wardData.get();
            newRealEstate.setWards(_ward);
        } else {
            newRealEstate.setWards(null);
        }

        if (streetData.isPresent()) {
            CStreet _street = streetData.get();
            newRealEstate.setStreet(_street);
        } else {
            newRealEstate.setStreet(null);
        }

        if (projectData.isPresent()) {
            CProject _project = projectData.get();
            newRealEstate.setProject(_project);
        } else {
            newRealEstate.setProject(null);
        }

        if (customerData.isPresent()) {
            CCustomer _customer = customerData.get();
            newRealEstate.setCustomer(_customer);
        } else {
            newRealEstate.setCustomer(null);
        }

        if (employeeData.isPresent()) {
            CEmployees _employee = employeeData.get();
            newRealEstate.setEmployee(_employee);
        } else {
            newRealEstate.setCustomer(null);
        }

        CRealEstate savedRealEstate = pRealEstateRepository.save(newRealEstate);
        return savedRealEstate;
    }
}
