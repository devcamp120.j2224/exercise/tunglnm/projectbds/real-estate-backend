package com.devcamp.project.realestate.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.project.realestate.model.CRealEstate;
import org.springframework.stereotype.Repository;

@Repository
public interface IRealEstateRepository extends JpaRepository<CRealEstate, Integer> {
    
    @Query(value = "SELECT * FROM `realestate` LIMIT 100 OFFSET 0 ;", nativeQuery = true)
    List<CRealEstate> getAllRealEstate();

    @Query(value = "SELECT * FROM `realestate` WHERE `project_id` = :projectId LIMIT 6 OFFSET 0 ;", nativeQuery = true)
    List<CRealEstate> getRealEstateByProjectId(@Param("projectId") Integer projectId);

    @Query(value = "SELECT * FROM `realestate` WHERE `project_id` = :projectId ;", nativeQuery = true)
    List<CRealEstate> getAllRealEstateByProjectId(@Param("projectId") Integer projectId);

    @Query(value = "SELECT * FROM `realestate` WHERE `employee_EmployeeID` = :employeeId ;", nativeQuery = true)
    List<CRealEstate> getRealEstateByEmployeeId(@Param("employeeId") Integer employeeId);

    @Query(value = "SELECT * FROM `realestate` WHERE `employee_EmployeeID` = :employeeId AND `status` = 'Y' ORDER BY `date_create` DESC LIMIT 9 OFFSET 0 ;", nativeQuery = true)
    List<CRealEstate> getRealEstateByEmployeeIdLimit9(@Param("employeeId") Integer employeeId);

    @Query(value = "SELECT * FROM `realestate` WHERE `employee_EmployeeID` = :employeeId AND `status` = 'N' ;", nativeQuery = true)
    List<CRealEstate> getFilterStatusRealEstateByEmployeeId(@Param("employeeId") Integer employeeId);

    @Query(value = "SELECT * FROM `realestate` WHERE `status` = 'N' ;", nativeQuery = true)
    List<CRealEstate> getAllRealEstateFilterStatus();
   
    @Query(value = "SELECT * FROM `realestate` WHERE `status` = 'Y' ORDER BY `date_create` DESC LIMIT 9 OFFSET 0 ;", nativeQuery = true)
    List<CRealEstate> getCurrentListRealEstate();
}
