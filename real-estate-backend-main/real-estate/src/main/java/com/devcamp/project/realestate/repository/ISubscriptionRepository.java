package com.devcamp.project.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.project.realestate.model.CSubscription;
import org.springframework.stereotype.Repository;

@Repository
public interface ISubscriptionRepository extends JpaRepository<CSubscription, Integer> {
    
}
