package com.devcamp.project.realestate.model;

import javax.persistence.*;

@Entity
@Table(name = "subscriptions")
public class CSubscription {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "user")
    private String user;

    @Column(name = "endpoint", nullable = false)
    private String endpoint;

    @Column(name = "publickey", nullable = false)
    private String publickey;

    @Column(name = "authenticationtoken", nullable = false)
    private String authenticationtoken;

    @Column(name = "contentencoding", nullable = false)
    private String contentencoding;

    public CSubscription() {
    }

    public CSubscription(int id, String user, String endpoint, String publickey, String authenticationtoken,
            String contentencoding) {
        this.id = id;
        this.user = user;
        this.endpoint = endpoint;
        this.publickey = publickey;
        this.authenticationtoken = authenticationtoken;
        this.contentencoding = contentencoding;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getPublickey() {
        return publickey;
    }

    public void setPublickey(String publickey) {
        this.publickey = publickey;
    }

    public String getAuthenticationtoken() {
        return authenticationtoken;
    }

    public void setAuthenticationtoken(String authenticationtoken) {
        this.authenticationtoken = authenticationtoken;
    }

    public String getContentencoding() {
        return contentencoding;
    }

    public void setContentencoding(String contentencoding) {
        this.contentencoding = contentencoding;
    }
    

}
