package com.devcamp.project.realestate.service;

import java.util.*;

import com.devcamp.project.realestate.model.*;
import com.devcamp.project.realestate.repository.IStreetRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StreetService {
    @Autowired
    IStreetRepository pStreetRepository;

    public List<CStreet> getStreetList() {
        List<CStreet> streetList = new ArrayList<CStreet>();
        pStreetRepository.findAll().forEach(streetList::add);
        return streetList;
    }

    public Object createStreetObj(CStreet cStreet, Optional<CDistrict> districtData) {

        CStreet newStreet = new CStreet();
        newStreet.setName(cStreet.getName());
        newStreet.setPrefix(cStreet.getPrefix());

        CDistrict _district = districtData.get();
        newStreet.set_district(_district);

        CStreet savedStreet = pStreetRepository.save(newStreet);
        return savedStreet;
    }

    public Object updateStreetObj(CStreet cStreet, Optional<CStreet> streetData) {

        CStreet newStreet = streetData.get();
        newStreet.setName(cStreet.getName());
        newStreet.setPrefix(cStreet.getPrefix());

        CStreet savedStreet = pStreetRepository.save(newStreet);
        return savedStreet;
    }

}
