package com.devcamp.project.realestate.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.project.realestate.model.CAdvise;
import com.devcamp.project.realestate.repository.IAdviseRepository;
import com.devcamp.project.realestate.service.AdviseService;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class AdviseController {
    
    @Autowired
    AdviseService pAdviseService;

    @Autowired
    IAdviseRepository pAdviseRepository;

    @GetMapping("/advises")
    @PreAuthorize("hasAnyRole('ADMIN','HOMESELLER')")
    public ResponseEntity<Object> getAllAdvise(){
        if(!pAdviseService.getAllAdvise().isEmpty()){
            return new ResponseEntity<>(pAdviseService.getAllAdvise(), HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/advises/status")
    @PreAuthorize("hasAnyRole('ADMIN','HOMESELLER')")
    public ResponseEntity<Object> getAllAdviseFilterStatus() {
        if (!pAdviseRepository.getAllAdviseFilterStatus().isEmpty()) {
            return new ResponseEntity<>(pAdviseRepository.getAllAdviseFilterStatus(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
    @GetMapping("/advise/status")
    @PreAuthorize("hasAnyRole('ADMIN','HOMESELLER')")
    public ResponseEntity<Object> getAllAdviseFilterStatusY() {
        if (!pAdviseRepository.getAllAdviseFilterStatusY().isEmpty()) {
            return new ResponseEntity<>(pAdviseRepository.getAllAdviseFilterStatusY(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/advises")
    public ResponseEntity<Object> createAdvise(@RequestBody CAdvise cAdvise){
        try {
            return new ResponseEntity<>(pAdviseService.createAdvise(cAdvise),HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/advises/{id}")
    @PreAuthorize("hasAnyRole('ADMIN','HOMESELLER')")
    public ResponseEntity<Object> updateAdvise(@PathVariable("id") int id ,@RequestBody CAdvise cAdvise){
        try {
            Optional<CAdvise> adviseData = pAdviseRepository.findById(id);
            if(adviseData.isPresent()){
                return new ResponseEntity<>(pAdviseService.updateAdvise(cAdvise, adviseData),HttpStatus.OK);
            }
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified Advise: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>("Advise NOT FOUND!", HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/advises/{id}")
    @PreAuthorize("hasAnyRole('ADMIN','HOMESELLER')")
    public ResponseEntity<Object> deleteAdviseId(@PathVariable("id") int id){
        try {
            pAdviseRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
