package com.devcamp.project.realestate.service;

import java.util.*;

import com.devcamp.project.realestate.model.*;
import com.devcamp.project.realestate.repository.IDistrictRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DistrictService {
    @Autowired
    IDistrictRepository pDistrictRepository;

    public List<CDistrict> getDistrictList() {
        List<CDistrict> districtList = new ArrayList<CDistrict>();
        pDistrictRepository.findAll().forEach(districtList::add);
        return districtList;
    }

    public Object createDistrictObj(CDistrict cDistrict, Optional<CProvince> provinceData) {

        CDistrict newDistrict = new CDistrict();
        newDistrict.setName(cDistrict.getName());
        newDistrict.setPrefix(cDistrict.getPrefix());

        CProvince _province = provinceData.get();
        newDistrict.set_province(_province);

        CDistrict savedDistrict = pDistrictRepository.save(newDistrict);
        return savedDistrict;
    }

    public Object updateDistrictObj(CDistrict cDistrict, Optional<CDistrict> districtData) {

        CDistrict newDistrict = districtData.get();
        newDistrict.setName(cDistrict.getName());
        newDistrict.setPrefix(cDistrict.getPrefix());

        CDistrict savedDistrict = pDistrictRepository.save(newDistrict);
        return savedDistrict;
    }


}