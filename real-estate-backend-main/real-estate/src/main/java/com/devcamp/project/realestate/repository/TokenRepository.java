package com.devcamp.project.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.project.realestate.entity.Token;

public interface TokenRepository extends JpaRepository<Token, Integer> {

    Token findByToken(String token);
}
