package com.devcamp.project.realestate.model;

import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "construction_contractor")
public class CConstructionContractor {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "name", nullable = false, length = 1000)
    private String name;

    @Column(name = "description",  length = 5000)
    private String description;

    @Column(name = "projects", length = 2000)
    private String projects;

    @Column(name = "address")
    private Integer address;

    @Column(name = "phone", length = 50)
    private String phone;

    @Column(name = "phone2", length = 50)
    private String phone2;

    @Column(name = "fax", length = 50)
    private String fax;

    @Column(name = "email", length = 200)
    private String email;

    @Column(name = "website", length = 1000)
    private String website;

    @Column(name = "note", columnDefinition="mediumtext")
    private String note;

    @OneToMany(targetEntity = CProject.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "constructionContractor_id")
    @JsonIgnore
    private Set<CProject> projects2;

    public CConstructionContractor() {
    }

    public CConstructionContractor(int id, String name, String description, String projects, Integer address, String phone,
            String phone2, String fax, String email, String website, String note) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.projects = projects;
        this.address = address;
        this.phone = phone;
        this.phone2 = phone2;
        this.fax = fax;
        this.email = email;
        this.website = website;
        this.note = note;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProjects() {
        return projects;
    }

    public void setProjects(String projects) {
        this.projects = projects;
    }

    public Integer getAddress() {
        return address;
    }

    public void setAddress(Integer address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<CProject> getProjects2() {
        return projects2;
    }

    public void setProjects2(Set<CProject> projects2) {
        this.projects2 = projects2;
    }
    

    
}
