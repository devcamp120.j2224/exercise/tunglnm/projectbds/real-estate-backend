package com.devcamp.project.realestate.controller;

import java.util.*;

import com.devcamp.project.realestate.model.*;
import com.devcamp.project.realestate.repository.*;
import com.devcamp.project.realestate.service.RealEstateService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class RealEstateController {

    @Autowired
    IRealEstateRepository pRealEstateRepository;

    @Autowired
    RealEstateService pRealEstateService;

    @GetMapping("/realEstates")
    @PreAuthorize("hasAnyRole('ADMIN', 'DEFAULT', 'HOMESELLER')")
    public ResponseEntity<Object> getAllRealEstate() {
        if (!pRealEstateRepository.getAllRealEstate().isEmpty()) {
            return new ResponseEntity<>(pRealEstateRepository.getAllRealEstate(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/currentListRealEstates")
    public ResponseEntity<Object> getCurrentListRealEstate() {
        if (!pRealEstateRepository.getCurrentListRealEstate().isEmpty()) {
            return new ResponseEntity<>(pRealEstateRepository.getCurrentListRealEstate(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/realEstates/project/{projectId}")
    public ResponseEntity<Object> getListRealEstateByProjectId(@PathVariable Integer projectId) {
        if (!pRealEstateRepository.getRealEstateByProjectId(projectId).isEmpty()) {
            return new ResponseEntity<>(pRealEstateRepository.getRealEstateByProjectId(projectId), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/realEstates/employee/{employeeId}")
    @PreAuthorize("hasAnyRole('ADMIN', 'HOMESELLER')")
    public ResponseEntity<Object> getListRealEstateByEmployeeId(@PathVariable Integer employeeId) {
        if (!pRealEstateRepository.getRealEstateByEmployeeId(employeeId).isEmpty()) {
            return new ResponseEntity<>(pRealEstateRepository.getRealEstateByEmployeeId(employeeId), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/realEstates/limit/employee/{employeeId}")
    @PreAuthorize("hasAnyRole('ADMIN', 'DEFAULT', 'HOMESELLER')")
    public ResponseEntity<Object> getList9RealEstateByEmployeeId(@PathVariable Integer employeeId) {
        if (!pRealEstateRepository.getRealEstateByEmployeeIdLimit9(employeeId).isEmpty()) {
            return new ResponseEntity<>(pRealEstateRepository.getRealEstateByEmployeeIdLimit9(employeeId), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/realEstates/status")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> getAllRealEstateFilterStatus() {
        if (!pRealEstateRepository.getAllRealEstateFilterStatus().isEmpty()) {
            return new ResponseEntity<>(pRealEstateRepository.getAllRealEstateFilterStatus(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/realEstates/employee/status/{employeeId}")
    @PreAuthorize("hasAnyRole('ADMIN', 'HOMESELLER')")
    public ResponseEntity<Object> getFilterStatusListRealEstateByEmployeeId(@PathVariable Integer employeeId) {
        if (!pRealEstateRepository.getFilterStatusRealEstateByEmployeeId(employeeId).isEmpty()) {
            return new ResponseEntity<>(pRealEstateRepository.getFilterStatusRealEstateByEmployeeId(employeeId), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/allRealEstates/project/{projectId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> getAllRealEstateByProjectId(@PathVariable Integer projectId) {
        if (!pRealEstateRepository.getAllRealEstateByProjectId(projectId).isEmpty()) {
            return new ResponseEntity<>(pRealEstateRepository.getAllRealEstateByProjectId(projectId), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/realEstates/{realEstateId}")
    public ResponseEntity<Object> getRealEstateById(@PathVariable Integer realEstateId) {
        if (pRealEstateRepository.findById(realEstateId).isPresent()) {
            return new ResponseEntity<>(pRealEstateRepository.findById(realEstateId).get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/realEstates/{provinceId}/{districtId}/{wardId}/{streetId}/{projectId}/{customerId}/{employeeId}")
    @PreAuthorize("hasAnyRole('ADMIN', 'DEFAULT', 'HOMESELLER')")
    public ResponseEntity<Object> createRealEstate(@PathVariable(required = false) Integer provinceId,
            @PathVariable(required = false) Integer districtId, @PathVariable(required = false) Integer wardId,
            @PathVariable(required = false) Integer streetId, @PathVariable(required = false) Integer projectId,
            @PathVariable(required = false) Integer customerId, @PathVariable(required = false) Integer employeeId,
            @RequestBody CRealEstate cRealEstate) {
        try {
            return new ResponseEntity<>(
                    pRealEstateService.createRealEstateObj(cRealEstate, provinceId, districtId, wardId,
                            streetId, projectId, customerId, employeeId),
                    HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Real Estate: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/realEstates/{realEstateId}/{provinceId}/{districtId}/{wardId}/{streetId}/{projectId}/{customerId}/{employeeId}")
    @PreAuthorize("hasAnyRole('ADMIN', 'DEFAULT', 'HOMESELLER')")
    public ResponseEntity<Object> updateRealEstate(@PathVariable Integer realEstateId,
            @PathVariable(required = false) Integer provinceId,
            @PathVariable(required = false) Integer districtId, @PathVariable(required = false) Integer wardId,
            @PathVariable(required = false) Integer streetId, @PathVariable(required = false) Integer projectId,
            @PathVariable(required = false) Integer customerId, @PathVariable(required = false) Integer employeeId,
            @RequestBody CRealEstate cRealEstate) {
        try {
            Optional<CRealEstate> realEstateData = pRealEstateRepository.findById(realEstateId);
            if (realEstateData.isPresent()) {
                return new ResponseEntity<>(
                        pRealEstateService.updateRealEstateObj(cRealEstate, realEstateData, provinceId, districtId,
                                wardId,
                                streetId, projectId, customerId, employeeId),
                        HttpStatus.OK);
            }
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified RealEstate: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/realEstates/{realEstateId}")
    @PreAuthorize("hasAnyRole('ADMIN', 'HOMESELLER')")
    public ResponseEntity<Object> deleteRealEstateById(@PathVariable("realEstateId") int realEstateId) {
        try {
            pRealEstateRepository.deleteById(realEstateId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
