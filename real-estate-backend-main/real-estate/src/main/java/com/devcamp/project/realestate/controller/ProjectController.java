package com.devcamp.project.realestate.controller;

import java.util.*;

import com.devcamp.project.realestate.model.*;
import com.devcamp.project.realestate.repository.*;
import com.devcamp.project.realestate.service.ProjectService;
import org.springframework.data.domain.Page;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class ProjectController {

    @Autowired
    IProjectRepository pProjectRepository;

    @Autowired
    ProjectService pProjectService;

    @GetMapping("/projects")
    @PreAuthorize("hasAnyRole('ADMIN', 'DEFAULT', 'HOMESELLER')")
    public ResponseEntity<Object> getAllProject() {
        if (!pProjectService.getProjectList().isEmpty()) {
            return new ResponseEntity<>(pProjectService.getProjectList(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/projectsList/page/{pageNumber}")
    public ResponseEntity<Object> getListProject(@PathVariable Integer pageNumber) {
        if (!pProjectRepository.getListOfProject(PageRequest.of(pageNumber, 9)).isEmpty()) {
            return new ResponseEntity<>(pProjectRepository.getListOfProject(PageRequest.of(pageNumber, 9)), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/totalPageNumberProjects")
    public int getTotalPageNumberProject() {
        Page<CProject> page = pProjectRepository.findAll(PageRequest.of(0, 9));
        return page.getTotalPages();
    }

    @GetMapping("/projects/{projectId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> getProjectById(@PathVariable Integer projectId) {
        if (pProjectRepository.findById(projectId).isPresent()) {
            return new ResponseEntity<>(pProjectRepository.findById(projectId).get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/projects/{provinceId}/{districtId}")
    public ResponseEntity<Object> getProjectByProvinceIdAndDistrictId(@PathVariable Integer provinceId, @PathVariable Integer districtId) {
        if (!pProjectRepository.getProjectByProvinceIdAndDistrictId(provinceId, districtId).isEmpty()) {
            return new ResponseEntity<>(pProjectRepository.getProjectByProvinceIdAndDistrictId(provinceId, districtId), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/projects/{provinceId}/{districtId}/{investorId}/{constructionContractorId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> createProject(@PathVariable(required = false) Integer provinceId,
            @PathVariable(required = false) Integer districtId, @PathVariable(required = false) Integer investorId,
            @PathVariable(required = false) Integer constructionContractorId,
            @RequestBody CProject cProject) {
        try {
            return new ResponseEntity<>(
                    pProjectService.createProjectObj(cProject, provinceId, districtId, investorId, constructionContractorId),
                    HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Project: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/projects/{projectId}/{provinceId}/{districtId}/{investorId}/{constructionContractorId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> updateProject(@PathVariable Integer projectId,
            @PathVariable(required = false) Integer provinceId, @PathVariable(required = false) Integer districtId,
            @PathVariable(required = false) Integer investorId, @PathVariable(required = false) Integer constructionContractorId,
            @RequestBody CProject cProject) {
        try {
            Optional<CProject> projectData = pProjectRepository.findById(projectId);
            if (projectData.isPresent()) {
                return new ResponseEntity<>(
                        pProjectService.updateProjectObj(cProject, projectData, provinceId, districtId, investorId, constructionContractorId),
                        HttpStatus.OK);
            }
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified Project: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/projects/{projectId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> deleteProjectById(@PathVariable("projectId") int projectId) {
        try {
            pProjectRepository.deleteById(projectId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
