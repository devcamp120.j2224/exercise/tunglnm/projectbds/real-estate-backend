package com.devcamp.project.realestate.controller;

import java.util.*;

import com.devcamp.project.realestate.model.*;
import com.devcamp.project.realestate.repository.*;
import com.devcamp.project.realestate.service.LocationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class LocationController {
    @Autowired
    ILocationRepository pLocationRepository;

    @Autowired
    LocationService pLocationService;

    @GetMapping("/locations")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> getLocationList() {
        if (!pLocationService.getLocationList().isEmpty()) {
            return new ResponseEntity<>(pLocationService.getLocationList(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/locations/{locationId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> getLocationById(@PathVariable Integer locationId) {
        if (pLocationRepository.findById(locationId).isPresent()) {
            return new ResponseEntity<>(pLocationRepository.findById(locationId).get(),
                    HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/locations")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> createLocation(@RequestBody CLocation cLocation) {
        try {
            return new ResponseEntity<>(pLocationService.createLocationObj(cLocation), HttpStatus.CREATED);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Location: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/locations/{locationId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> updateLocation(@PathVariable("locationId") Integer locationId,
            @RequestBody CLocation cLocation) {
        try {
            Optional<CLocation> locationData = pLocationRepository.findById(locationId);
            if (locationData.isPresent()) {
                return new ResponseEntity<>(pLocationService.updateLocationObj(cLocation, locationData), HttpStatus.OK);
            }
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified Location: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/locations/{locationId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> deleteLocationById(@PathVariable("locationId") int locationId) {
        try {
            pLocationRepository.deleteById(locationId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
