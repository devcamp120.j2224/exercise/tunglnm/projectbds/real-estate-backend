package com.devcamp.project.realestate.controller;

import java.util.*;

import com.devcamp.project.realestate.model.*;
import com.devcamp.project.realestate.repository.*;
import com.devcamp.project.realestate.service.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class WardController {
    @Autowired
    IProvinceRepository pProvinceRepository;

    @Autowired
    IDistrictRepository pDistrictRepository;

    @Autowired
    IWardRepository pWardRepository;

    @Autowired
    WardService pWardService;

    @GetMapping("/wards")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> getWardsList() {
        if (!pWardRepository.getWardList().isEmpty()) {
            return new ResponseEntity<>(pWardRepository.getWardList(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/wards1")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> getWardList1(){
        if(!pWardRepository.getWardList1().isEmpty()){
            return new ResponseEntity<>(pWardRepository.getWardList1(),HttpStatus.OK);
        }else{
            return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/wards/{wardId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> getWardById(@PathVariable Integer wardId) {
        if (pWardRepository.findById(wardId).isPresent()) {
            return new ResponseEntity<>(pWardRepository.findById(wardId).get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/wards/district/{districtId}")
    @PreAuthorize("hasAnyRole('ADMIN', 'DEFAULT', 'HOMESELLER')")
    public ResponseEntity<Object> getWardListByDistrictId(@PathVariable Integer districtId) {
        if (!pWardRepository.getWardListByDistrictId(districtId).isEmpty()) {
            return new ResponseEntity<>(pWardRepository.getWardListByDistrictId(districtId), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/wards/{districtId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> createWard(@PathVariable("districtId") int districtId, @RequestBody CWard cWard) {
        try {
            Optional<CDistrict> districtData = pDistrictRepository.findById(districtId);
            if (districtData.isPresent()) {
                return new ResponseEntity<>(pWardService.createWardObj(cWard, districtData),
                        HttpStatus.CREATED);
            }
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Ward: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/wards/{wardId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> updateWard(@PathVariable("wardId") Integer wardId, @RequestBody CWard cWard) {
        try {
            Optional<CWard> wardData = pWardRepository.findById(wardId);
            if (wardData.isPresent()) {
                return new ResponseEntity<>(pWardService.updateWardObj(cWard, wardData), HttpStatus.OK);
            }
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified Ward: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>("Ward NOT FOUND!", HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/wards/{wardId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> deleteWardById(@PathVariable Integer wardId) {
        try {
            pWardRepository.deleteById(wardId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
