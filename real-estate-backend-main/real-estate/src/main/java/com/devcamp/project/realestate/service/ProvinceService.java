package com.devcamp.project.realestate.service;

import java.util.*;

import com.devcamp.project.realestate.model.CProvince;
import com.devcamp.project.realestate.repository.IProvinceRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProvinceService {
    @Autowired
    IProvinceRepository pProvinceRepository;

    public List<CProvince> getProvinceList() {
        List<CProvince> provinceList = new ArrayList<CProvince>();
        pProvinceRepository.findAll().forEach(provinceList::add);
        return provinceList;
    }

    public Object createProvinceObj(CProvince cProvince) {

        CProvince newProvince = new CProvince();
        newProvince.setCode(cProvince.getCode());
        newProvince.setName(cProvince.getName());

        CProvince savedProvince = pProvinceRepository.save(newProvince);
        return savedProvince;
    }

    public Object updateProvinceObj(CProvince cProvince, Optional<CProvince> provinceData) {

        CProvince newProvince = provinceData.get();
        newProvince.setCode(cProvince.getCode());
        newProvince.setName(cProvince.getName());

        CProvince savedProvince = pProvinceRepository.save(newProvince);
        return savedProvince;
    }
}
